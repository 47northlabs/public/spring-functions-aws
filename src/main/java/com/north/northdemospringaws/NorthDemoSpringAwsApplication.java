package com.north.northdemospringaws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NorthDemoSpringAwsApplication {

    public static void main(String[] args) {
        SpringApplication.run(NorthDemoSpringAwsApplication.class, args);
    }

}
